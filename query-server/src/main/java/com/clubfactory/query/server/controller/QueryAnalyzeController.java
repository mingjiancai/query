package com.clubfactory.query.server.controller;

import com.clubfactory.query.client.dto.request.SearchQpDTO;
import com.clubfactory.query.client.dto.search.SearchRequestDTO;
import com.clubfactory.query.server.service.impl.QueryAnalyzeServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author: cmj
 * @Description:
 * @Date: 2019/2/26
 */
@RestController
@Slf4j
public class QueryAnalyzeController {
    @RequestMapping(value = "/check/", method = RequestMethod.GET)
    public Boolean checkHealth() {
        return true;
    }
    /**
     * 用于Jenkins确认状态
     * @return
     */
    @RequestMapping(value = "/health_check", method = RequestMethod.GET)
    public String healthCheck() {

        return "success";

    }


    @Autowired
    QueryAnalyzeServiceImpl queryAnalyzeService;

    @PostMapping("/query/analyze")
    public SearchQpDTO getQpResponse(@RequestBody SearchRequestDTO searchRequestDTO) {
        SearchQpDTO queryDTO = queryAnalyzeService.getQueryDTO(searchRequestDTO);
        return queryDTO;
    }

}
