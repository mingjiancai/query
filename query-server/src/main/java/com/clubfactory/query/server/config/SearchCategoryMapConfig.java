package com.clubfactory.query.server.config;

import com.clubfactory.query.client.dto.search.SearchCategoryDTO;
import com.clubfactory.query.core.utils.CsvUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Configuration
public class SearchCategoryMapConfig {
    public static Map<String, List<Long>> searchCategoryMap = new HashMap<>();

    public static Map<String, List<Long>> searchLimitCategoryMap = new HashMap<>();


    @Value("classpath:search_category.csv")
    private Resource searchCategoryCsv;


    /**
     * 本地获取类目映射
     *
     * @return
     */
    public List<SearchCategoryDTO> getSearchCategoryDTOList() {
        try {
            return CsvUtil.toNormalObject(searchCategoryCsv.getInputStream(), SearchCategoryDTO.class);
        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
            return null;
        }
    }

    @PostConstruct
    public Map<String, List<Long>> getSearchCategoryMap() {
        List<SearchCategoryDTO> searchCategoryDTOList = getSearchCategoryDTOList();
        if (null != searchCategoryDTOList) {
            searchCategoryDTOList.forEach(searchCategoryDTO -> {
                if("等于".equals(searchCategoryDTO.getRelation())) {
                    try {
                        setCategoryMap(searchCategoryMap,searchCategoryDTO);
                    }
                    catch (Exception e) {
                        log.error(ExceptionUtils.getStackTrace(e));
                    }
                }
            });
        }
        return searchCategoryMap;
    }

    @PostConstruct
    public Map<String, List<Long>> getSearchLimitCategoryMap() {
        List<SearchCategoryDTO> searchCategoryDTOList=getSearchCategoryDTOList();
        if (null != searchCategoryDTOList) {
            searchCategoryDTOList.forEach(searchCategoryDTO -> {
                if("小于".equals(searchCategoryDTO.getRelation())) {
                    try {
                        setCategoryMap(searchLimitCategoryMap,searchCategoryDTO);
                    }
                    catch (Exception e) {
                        log.error(ExceptionUtils.getStackTrace(e));
                    }
                }
            });
        }
        return searchLimitCategoryMap;
    }

    /**
     * 构建类目map
     * @param categoryMap
     * @param searchCategoryDTO
     */
    private void setCategoryMap(Map<String, List<Long>> categoryMap, SearchCategoryDTO searchCategoryDTO) {
        categoryMap.put(
                String.format("%s_%s", searchCategoryDTO.getSearchKey().toLowerCase(), "men"),
                Arrays.stream(searchCategoryDTO.getMaleCategoryId().replaceAll(" ","").split(",")).filter(StringUtils::isNotEmpty).map(Long::parseLong).collect(Collectors.toList())
        );
        categoryMap.put(
                String.format("%s_%s", searchCategoryDTO.getSearchKey().toLowerCase(), "women"),
                Arrays.stream(searchCategoryDTO.getFemaleCategoryId().replaceAll(" ","").split(",")).filter(StringUtils::isNotEmpty).map(Long::parseLong).collect(Collectors.toList())
        );
        categoryMap.put(
                String.format("%s_%s", searchCategoryDTO.getSearchKey().toLowerCase(), "all"),
                Arrays.stream(searchCategoryDTO.getAllCategoryId().replaceAll(" ","").split(",")).filter(StringUtils::isNotEmpty).map(Long::parseLong).collect(Collectors.toList())
        );

    }
}
