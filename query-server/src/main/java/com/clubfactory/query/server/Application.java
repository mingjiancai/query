package com.clubfactory.query.server;

import com.clubfactory.boot.autoconfigure.dubbo.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.clubfactory.query")
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
