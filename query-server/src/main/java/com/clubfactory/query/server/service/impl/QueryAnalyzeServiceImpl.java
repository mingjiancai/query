package com.clubfactory.query.server.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.clubfactory.cat.integration.dubbo.util.ExceptionUtil;
import com.clubfactory.query.client.dto.analyze.*;
import com.clubfactory.query.client.dto.request.*;
import com.clubfactory.query.client.dto.search.SearchDTO;
import com.clubfactory.query.client.dto.search.SearchRequestDTO;
import com.clubfactory.query.client.dto.search.SkuFilterDTO;
import com.clubfactory.query.core.dto.consts.SearchRequestConst;
import com.clubfactory.query.core.dto.enums.GenderEnum;
import com.clubfactory.query.core.dto.result.Result;
import com.clubfactory.query.core.utils.HttpClientUtil;
import com.clubfactory.query.core.utils.TermUtil;
import com.clubfactory.query.server.service.QueryAnalyzeService;
import com.clubfactory.query.server.service.QueryChangeService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StopWatch;

import javax.annotation.Resource;
import java.lang.reflect.Type;
import java.util.*;

/**
 * @Author: cmj
 * @Description:
 * @Date: 2019/2/27
 */
@Service
@Slf4j
public class QueryAnalyzeServiceImpl implements QueryAnalyzeService {

    @Override
    public List<String> spliteTerm(String term) {
        String[] split = term.toLowerCase().split("\\s+");
        List<String> list = Arrays.asList(split);
        return list;
    }


    public static final String WORD_EDGE = "\\b";
    private static final int LENGTH = 150;
    @Autowired
    SearchRequestConst searchRequestConst;
    @Resource
    QueryAnalyzeService queryAnalyzeService;

    @Resource
    QueryChangeService queryChangeService;

    @Resource
    TermUtil termUtil;

    private static final Type WORD_ANALYZE_RESULT_TYPE = new TypeReference<Result<QueryAnalyzeResponse>>(){}.getType();

    /**
     * 构造query请求对象
     *
     * @param searchRequestDTO
     * @return
     */
    @Override
    public SearchQpDTO getQueryDTO(SearchRequestDTO searchRequestDTO) {
        log.info("获取召回入参:{}",JSONObject.toJSONString(searchRequestDTO));
        SearchQpDTO queryDTO = new SearchQpDTO();
        queryDTO.setVersion(searchRequestDTO.getNewVersion());
        //获取人工类目信息、改写query
        SearchDTO searchDTO = new SearchDTO(searchRequestDTO);
        searchDTO = searchPreProcess(searchDTO);
        CategoryDTO categoryDTO = queryChangeService.getCategoryMapInfo(searchRequestDTO, searchDTO);

        //获取query分析结果
        QueryAnalyzeResponse queryAnalyzeResponse = getQueryAnalyzeResponse(searchRequestDTO, searchDTO, searchRequestDTO.getPvid());
        //bebug

        queryDTO.setDebug(searchDTO.getIsBebug());
        //用户信息
        UserInfoDTO userInfoDTO = getUserInfo(searchDTO);
        queryDTO.setUserInfo(userInfoDTO);
        //过滤条件
        UserFilterDTO userFilterDTO = getUserFilterDTO(searchDTO);
        queryDTO.setUserFilter(userFilterDTO);
        //类目信息(预测类目)
        CategoryPredictDTO categoryPredictDTO = getCategoryInfo(categoryDTO);
        queryDTO.setCategoryPredict(categoryPredictDTO);

        QueryAnalyzeDTO queryAnalyzeDTO = getQueryAnalyzeDTO(queryAnalyzeResponse);
        queryDTO.setQueryAnalyze(queryAnalyzeDTO);
        queryDTO.setOriginQuery(searchRequestDTO.getKey());
        queryDTO.setSortMap(searchDTO.getSortMap());
        queryDTO.setPvid(searchRequestDTO.getPvid());
        return queryDTO;
    }

    /**
     * 获取query分析DTO
     *
     * @return
     */
    private QueryAnalyzeDTO getQueryAnalyzeDTO(QueryAnalyzeResponse queryAnalyzeResponse) {
        QueryAnalyzeDTO queryAnalyzeDTO = new QueryAnalyzeDTO();
        queryAnalyzeDTO.setBrand(queryAnalyzeResponse.getBrand());
        queryAnalyzeDTO.setColor(queryAnalyzeResponse.getColor());
        queryAnalyzeDTO.setProduct(queryAnalyzeResponse.getProduct());
        queryAnalyzeDTO.setSize(queryAnalyzeResponse.getSize());
        AgeDTO ageDTO = new AgeDTO();
        YearDTO yearDTO = new YearDTO();
        if (null != queryAnalyzeResponse.getAge() && null != queryAnalyzeResponse.getAge().getYears() &&
                queryAnalyzeResponse.getAge().getYears().getFrom() != null) {
            try {
                yearDTO.setFrom(Integer.valueOf(queryAnalyzeResponse.getAge().getYears().getFrom()));
            } catch (Exception e) {

            }
        }
        if (null != queryAnalyzeResponse.getAge() && null != queryAnalyzeResponse.getAge().getYears() &&
                queryAnalyzeResponse.getAge().getYears().getTo() != null) {
            try {
                yearDTO.setTo(Integer.valueOf(queryAnalyzeResponse.getAge().getYears().getTo()));
            } catch (Exception e) {

            }
        }
        if (null != queryAnalyzeResponse.getAge() && null != queryAnalyzeResponse.getAge().getType()) {
            ageDTO.setTypes(queryAnalyzeResponse.getAge().getType());
        }
        ageDTO.setYear(yearDTO);
        queryAnalyzeDTO.setAge(ageDTO);
        PriceFilterDTO priceFilterDTO = new PriceFilterDTO();
        if (null != queryAnalyzeResponse.getPrice() && null != queryAnalyzeResponse.getPrice().getFrom()) {
            try {
                priceFilterDTO.setFrom(Double.valueOf(queryAnalyzeResponse.getPrice().getFrom()));
            } catch (Exception e) {

            }
        }
        if (null != queryAnalyzeResponse.getPrice() && null != queryAnalyzeResponse.getPrice().getTo()) {
            try {
                priceFilterDTO.setFrom(Double.valueOf(queryAnalyzeResponse.getPrice().getFrom()));
            } catch (Exception e) {

            }
        }
        queryAnalyzeDTO.setPrice(priceFilterDTO);
        List<RewriteDTO> rewriteDTOList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(queryAnalyzeResponse.getRewrites())) {
            for (Rewrites rewriteDTO : queryAnalyzeResponse.getRewrites()) {
                RewriteDTO rewriteDTO1 = new RewriteDTO();
                rewriteDTO1.setScore(Double.valueOf(rewriteDTO.getScore()));
                QueryKeyDTO queryKeyDTO = new QueryKeyDTO();
                queryKeyDTO.setShould(rewriteDTO.getQuery().getShould());
                queryKeyDTO.setMust(rewriteDTO.getQuery().getMust());
                queryKeyDTO.setFuzz(rewriteDTO.getQuery().getFuzz());
                rewriteDTO1.setQuery(queryKeyDTO);
                rewriteDTOList.add(rewriteDTO1);
            }
        }
        queryAnalyzeDTO.setRewrites(rewriteDTOList);

        return queryAnalyzeDTO;

    }

    /**
     * 获取组装的类目信息
     *
     * @param categoryDTO
     * @return
     */
    private CategoryPredictDTO getCategoryInfo(CategoryDTO categoryDTO) {
        CategoryPredictDTO categoryPredictDTO = new CategoryPredictDTO();
        categoryPredictDTO.setRootCategoryPredict(categoryDTO.getRootCategoryPredict());
        categoryPredictDTO.setSecondCategoryPredict(categoryDTO.getSecondCategoryPredict());
        categoryPredictDTO.setLeafCategoryPredict(categoryDTO.getLeafCategoryPredict());
        categoryPredictDTO.setRootBoost(5000d);
        categoryPredictDTO.setSecondBoost(10000d);
        categoryPredictDTO.setLeafBoost(20000d);
        return categoryPredictDTO;
    }


    /**
     * 获取用户过滤条件
     *
     * @param searchDTO
     * @return
     */
    private UserFilterDTO getUserFilterDTO(SearchDTO searchDTO) {
        UserFilterDTO userFilterDTO = new UserFilterDTO();
        List color = new ArrayList();
        List size = new ArrayList();
        List<com.clubfactory.query.client.dto.search.PriceFilterDTO> price_filters = searchDTO.getPrice_filters();
        if (!CollectionUtils.isEmpty(price_filters)) {
            com.clubfactory.query.client.dto.search.PriceFilterDTO priceFilterDTO = new com.clubfactory.query.client.dto.search.PriceFilterDTO();
            Double lte = 0.0;

            Double gte = 100000.0;
            for (com.clubfactory.query.client.dto.search.PriceFilterDTO price_filter : price_filters) {
                if (price_filter.getGte() != null && price_filter.getGte() < gte) {
                    gte = price_filter.getGte();
                }
                if (price_filter.getGte() == null) {
                    gte = 0.0;
                }
                if (price_filter.getLte() != null && price_filter.getLte() > lte) {
                    lte = price_filter.getLte();
                }
                if (price_filter.getLte() == null) {
                    lte = 100000.0;
                }
            }
            priceFilterDTO.setGte(gte);
            priceFilterDTO.setLte(lte);
            PriceFilterDTO priceFilterDTO1 = new PriceFilterDTO();
            priceFilterDTO1.setFrom(priceFilterDTO.getGte());
            priceFilterDTO1.setTo(priceFilterDTO.getLte());
            userFilterDTO.setPrice(priceFilterDTO1);
        }
        if (!CollectionUtils.isEmpty(searchDTO.getSku_filters())) {
            for (SkuFilterDTO skuFilterDTO : searchDTO.getSku_filters()) {
                if ("color".equals(skuFilterDTO.getAttrName().toLowerCase())) {
                    color.add(skuFilterDTO.getAttrValue());
                } else if ("size".equals(skuFilterDTO.getAttrName().toLowerCase())) {
                    size.add(skuFilterDTO.getAttrValue());
                }
            }
        }

        userFilterDTO.setColor(color);
        userFilterDTO.setSize(size);
        List<Integer> category = new ArrayList<>();
        if (!CollectionUtils.isEmpty(searchDTO.getCategory())) {
            searchDTO.getCategory().forEach(id -> category.add(Integer.valueOf(id.toString())));
        }
        userFilterDTO.setCategory(category);
        return userFilterDTO;
    }

    /**
     * 获取用户信息
     *
     * @param searchDTO
     * @return
     */
    private UserInfoDTO getUserInfo(SearchDTO searchDTO) {
        UserInfoDTO userInfoDTO = new UserInfoDTO();
        userInfoDTO.setCid(searchDTO.getCid());
        userInfoDTO.setUid(searchDTO.getUserId());
        userInfoDTO.setCountry(searchDTO.getCountry_code());
        userInfoDTO.setGender(com.clubfactory.query.client.dto.enums.GenderEnum.getByString(searchDTO.getSex()));
        return userInfoDTO;
    }

    private QueryAnalyzeResponse getQueryAnalyzeResponse(SearchRequestDTO searchRequestDTO, SearchDTO searchDTO, String pvid) {
        QueryAnalyzeRequest queryAnalyzeRequest = getQueryAnalyzeRequestDTO(searchRequestDTO, searchDTO);
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        QueryAnalyzeResponse queryAnalyzeResponse = getQueryAnalyzeResponse(queryAnalyzeRequest);
        stopWatch.stop();
        log.info("query分析耗时:{},pvid:{}", stopWatch.getTotalTimeSeconds() * 1000, pvid);
        return queryAnalyzeResponse;
    }

    public QueryAnalyzeResponse getQueryAnalyzeResponse(QueryAnalyzeRequest queryAnalyzeRequest) {
        QueryAnalyzeResponse queryAnalyzeResponse = new QueryAnalyzeResponse();
        try {
            String json=JSON.toJSONString(queryAnalyzeRequest);
            log.info("query分析，入参:{}"+json);
            String s= HttpClientUtil.post(searchRequestConst.getQueryAnalyze(),json);
            Result<QueryAnalyzeResponse> result=JSON.parseObject(s,WORD_ANALYZE_RESULT_TYPE);
            queryAnalyzeResponse=result.getData();
        } catch (Exception e) {
            log.error("获取query分析结果失败,e:", e.getMessage());
        }
        return queryAnalyzeResponse;
    }

    /**
     * 构造query分析查询对象
     *
     * @param searchRequestDTO
     * @param searchDTO
     * @return
     */
    private QueryAnalyzeRequest getQueryAnalyzeRequestDTO(SearchRequestDTO searchRequestDTO, SearchDTO searchDTO) {
        Ab ab = new Ab();
        ab.setExpid(searchDTO.getNewVersion());
        ab.setBucket_id(null);
        User user = new User();
        user.setCid(searchDTO.getCid());
        user.setCourtry(searchDTO.getCountry_code());
        user.setGender(Objects.equals(searchRequestDTO.getSex(), "") ? "ALL" : searchRequestDTO.getSex());

        List<SkuFilterDTO> skuFilters = searchDTO.getSku_filters();
        List color = new ArrayList();
        List size = new ArrayList();
        if (!CollectionUtils.isEmpty(skuFilters)) {
            for (SkuFilterDTO skuFilterDTO : skuFilters) {
                if ("color".equals(skuFilterDTO.getAttrName())) {
                    color.add(skuFilterDTO.getAttrValue());
                } else if ("size".equals(skuFilterDTO.getAttrName())) {
                    size.add(skuFilterDTO.getAttrValue());
                }
            }
        }

        SearchDTO searchDTOV2 = new SearchDTO(searchRequestDTO);
        String queryWithSex = searchDTOPreProcessV2(searchDTOV2);
        Sku sku = new Sku();
        sku.setColor(color);
        sku.setSize(size);
        Input input = new Input();
        if (!CollectionUtils.isEmpty(searchDTO.getCategory())) {
            input.setCategory(searchDTO.getCategory());
        } else {
            input.setCategory(new ArrayList<>());
        }
        input.setSku(sku);
        input.setPred_gender(searchDTO.getSex());
        QueryAnalyzeRequest queryAnalyzeRequest = new QueryAnalyzeRequest();
        queryAnalyzeRequest.setAb(ab);
        queryAnalyzeRequest.setInput(input);
        queryAnalyzeRequest.setQuery(queryWithSex);
        queryAnalyzeRequest.setUser(user);
        return queryAnalyzeRequest;
    }

    /**
     * 请求词预处理
     *
     * @param searchDTO
     * @return
     */
    public String searchDTOPreProcessV2(SearchDTO searchDTO) {
        /** keyword预处理 */
        String keyword = searchDTO.getKey();
        keyword = QueryChangeServiceImpl.removeURL(keyword);
        keyword = StringUtils.lowerCase(keyword);
        keyword = QueryChangeServiceImpl.removeStopWords(keyword);

        /** 性别预处理 */

        StringBuilder keywordSbWithoutGender = new StringBuilder();
        /** 如果搜索词中有明确的指向，则以搜索词为准 */
        for (String s : keyword.split(WORD_EDGE)) {
            keywordSbWithoutGender.append(s);
        }
        String keywordWithoutGender = keywordSbWithoutGender.toString();
        if (!StringUtils.isBlank(keywordWithoutGender)) {
            keyword = StringUtils.trim(keywordWithoutGender);
        }

        keyword = QueryChangeServiceImpl.removeExtraBlanks(keyword);


        return keyword;
    }

    /**
     * 预处理
     *
     * @param searchDTO
     * @return
     */
    private SearchDTO searchPreProcess(SearchDTO searchDTO) {
        String key = queryChangeService.searchPreProcess(searchDTO);
        GenderEnum sex = queryChangeService.getSex(searchDTO);
        searchDTO.setKey(key);
        searchDTO.setSex(sex.getKey());
        return searchDTO;}

    public QueryAnalyzeResponse queryAnalyze(QueryAnalyzeRequest queryAnalyzeRequest) {
        String json = "{\n" +
                "    \"ab\": {\n" +
                "        \"expid\": 1,\n" +
                "        \"bucket_id\": 1\n" +
                "    },\n" +
                "    \"user\": {\n" +
                "        \"courtry\": \"in\",\n" +
                "        \"gender\": \"woman\",\n" +
                "        \"ip\": \"201.111.11.11\",\n" +
                "        \"city\": \"deli\",\n" +
                "        \"uid\": \"111\",\n" +
                "        \"cid\": \"cookie1\"\n" +
                "    },\n" +
                "    \"input\": {\n" +
                "        \"category\": [\n" +
                "            60101,\n" +
                "            60102\n" +
                "        ],\n" +
                "        \"sku\": {\n" +
                "            \"color\": [\n" +
                "                \"red\"\n" +
                "            ],\n" +
                "            \"size\": [\n" +
                "                \"xl\"\n" +
                "            ]\n" +
                "        },\n" +
                "        \"pred_gender\": \"woman\"\n" +
                "    },\n" +
                "    \"query\": \"top woman stylish\"\n" +
                "}\n";
        queryAnalyzeRequest = JSON.parseObject(json, QueryAnalyzeRequest.class);
        QueryAnalyzeResponse queryAnalyzeResponse = new QueryAnalyzeResponse();
        try {

            queryAnalyzeResponse = lostTerm(queryAnalyzeRequest);
        } catch (Exception e) {
            log.error("query分析失败，e:{}", ExceptionUtil.getStackTrace(e));
        }
        return queryAnalyzeResponse;

    }

    private QueryAnalyzeResponse lostTerm(QueryAnalyzeRequest queryAnalyzeRequest) {
        QueryAnalyzeResponse queryAnalyzeResponse = new QueryAnalyzeResponse();
        String gender = queryAnalyzeRequest.getUser().getGender();
        if (StringUtils.isNotBlank(gender)) {
            gender = "all";
        }
        // TODO: 2019/2/27 性别对应key
        String oldQuery = queryAnalyzeRequest.getQuery();
        JSONObject tagTermJsonObject = termUtil.getTagTerm("search:getQueryTaggingRedisKey", oldQuery);
        String price_str = tagTermJsonObject.getOrDefault("price_str", "").toString();
        String age_str = tagTermJsonObject.getOrDefault("age_str", "").toString();
        String stop = tagTermJsonObject.getOrDefault("stop", "").toString();
        Set<String> set = new HashSet<String>() {{
            add(price_str);
            add(age_str);
            add(stop);
        }};
        if (StringUtils.isNotBlank(oldQuery)) {
            if (oldQuery.length() > LENGTH) {
                oldQuery = oldQuery.substring(0, LENGTH);
            }
            List<String> list = queryAnalyzeService.spliteTerm(oldQuery);
            if (!CollectionUtils.isEmpty(list) && !CollectionUtils.isEmpty(tagTermJsonObject)) {
                for (String term : list) {
                    if (set.contains(term)) {
                        list.remove(term);
                    }
                }
            }
        }
        return queryAnalyzeResponse;
    }

}
