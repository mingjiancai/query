package com.clubfactory.query.server.service;

import com.clubfactory.query.client.dto.request.SearchQpDTO;
import com.clubfactory.query.client.dto.search.SearchRequestDTO;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: cmj
 * @Description:
 * @Date: 2019/2/27
 */
public interface QueryAnalyzeService {

    List<String> spliteTerm(String term);

    SearchQpDTO getQueryDTO(SearchRequestDTO searchRequestDTO);
}
