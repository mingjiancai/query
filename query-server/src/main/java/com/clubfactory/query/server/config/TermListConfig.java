package com.clubfactory.query.server.config;

import com.clubfactory.query.core.dto.term.corrent.Term;
import com.clubfactory.query.core.dto.term.corrent.TermError;
import com.clubfactory.query.core.utils.CsvUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: cmj
 * @Description:
 * @Date: 2019/2/26
 */
@Slf4j
@Configuration
public class TermListConfig {
    @Value("classpath:term.csv")
    private Resource termCsv;
    @Value("classpath:term-error.csv")
    private Resource termErrorCsv;

    public static List<Term> termList = new ArrayList<>();
    public static List<TermError> termErrorList = new ArrayList<>();

    @PostConstruct
    public void initTermList() {
        try {
            termList = CsvUtil.toNormalObject(termCsv.getInputStream(), Term.class);
            termErrorList = CsvUtil.toNormalObject(termErrorCsv.getInputStream(), TermError.class);
        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
        }
    }

}
