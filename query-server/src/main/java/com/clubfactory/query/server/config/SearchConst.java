package com.clubfactory.query.server.config;

import com.clubfactory.query.core.dto.enums.GenderEnum;

import java.util.HashMap;
import java.util.Map;
public class SearchConst {

    public static Map<String, GenderEnum> sexMap = new HashMap<>();

    public final static Float scoreConst = 0.3f;
    public final static Float scoreConstAb = 1.3f;
    public final static String suggestField = "suggest_phrase";
    public final static String aggsField = "category_aggs";
    public final static Integer ContentItemList = 1;         // 商品列表，content为001时表示商品列表
    public final static Integer ContentSuggest = 2;          //建议，content为010时表示建议
    public final static Integer ContentAggs = 4;             //聚合，content为100时表示聚合
    public final static Double newArrivalWeight = 0.15;
    public final static Double grayWeight = 0.1;

    public final static String ITEM_SEARCH_0716 = "item_search_0716";

    static {
        sexMap.put("man", GenderEnum.Man);
        sexMap.put("men", GenderEnum.Man);
        sexMap.put("male", GenderEnum.Man);
        sexMap.put("mans", GenderEnum.Man);
        sexMap.put("mens", GenderEnum.Man);
        sexMap.put("boy", GenderEnum.Man);
        sexMap.put("boys", GenderEnum.Man);
        sexMap.put("males", GenderEnum.Man);
        sexMap.put("men's", GenderEnum.Man);
        sexMap.put("man's", GenderEnum.Man);
        sexMap.put("woman", GenderEnum.Woman);
        sexMap.put("women", GenderEnum.Woman);
        sexMap.put("female", GenderEnum.Woman);
        sexMap.put("womans", GenderEnum.Woman);
        sexMap.put("womens", GenderEnum.Woman);
        sexMap.put("girl", GenderEnum.Woman);
        sexMap.put("girls", GenderEnum.Woman);
        sexMap.put("ladies", GenderEnum.Woman);
        sexMap.put("lady's", GenderEnum.Woman);
        sexMap.put("females", GenderEnum.Woman);
        sexMap.put("women's", GenderEnum.Woman);
        sexMap.put("woman's", GenderEnum.Woman);
    }
}
