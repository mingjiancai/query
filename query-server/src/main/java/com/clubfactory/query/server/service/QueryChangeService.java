package com.clubfactory.query.server.service;

import com.clubfactory.query.client.dto.analyze.CategoryDTO;
import com.clubfactory.query.client.dto.search.SearchDTO;
import com.clubfactory.query.client.dto.search.SearchRequestDTO;
import com.clubfactory.query.core.dto.category.CategoryAndWeightDTO;
import com.clubfactory.query.core.dto.category.CategoryPredictResponseV2;
import com.clubfactory.query.core.dto.category.LeafCategoryPredictResult;
import com.clubfactory.query.core.dto.enums.GenderEnum;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @Author: cmj
 * @Description: query改写接口
 * @Date: 2019/1/11
 */
public interface QueryChangeService {

    /**
     * query预处理
     *
     * @param searchDTO
     * @return
     */
    String searchPreProcess(SearchDTO searchDTO);

    GenderEnum getSex(SearchDTO searchDTO);

    CategoryDTO getCategoryMapInfo(SearchRequestDTO searchRequestDTO, SearchDTO searchDTO);

    CategoryPredictResponseV2 getCategoryInfo(final String queryStr, final Optional<GenderEnum> originGender);

    LeafCategoryPredictResult getLeafCategoryInfo(final String queryStr, final Optional<GenderEnum> originGender, Integer newVersion);

    CategoryAndWeightDTO getCategoryAndWeightDTO();
}
