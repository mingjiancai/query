package com.clubfactory.query.server.config;

import org.springframework.context.annotation.Bean;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * @author shenjianan
 * @date 2018/7/26
 */
@Component
public class RestTemplateProvider {


    @Bean
    public RestTemplate restTemplate() {
        // 超时时间
        HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory();


        return new RestTemplate(httpRequestFactory);
    }

}
