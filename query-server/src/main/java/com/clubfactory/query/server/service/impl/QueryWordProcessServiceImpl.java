package com.clubfactory.query.server.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.clubfactory.query.client.dto.request.SearchQpDTO;
import com.clubfactory.query.client.service.QueryWordProcessService;
import com.clubfactory.query.server.service.QueryAnalyzeService;
import com.jiayun.data.strategy.platform.client.domain.search.request.SearchRequestDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Author: cmj
 * @Description:
 * @Date: 2019/3/1
 */
@Service(protocol = {"dubbo"},timeout = 2000)
@Slf4j
public class QueryWordProcessServiceImpl implements QueryWordProcessService {

    @Autowired
    QueryAnalyzeService queryAnalyzeService;

    /**
     * 构造query请求对象
     *
     * @param searchRequestDTO
     * @return
     */

    @Override
    public SearchQpDTO getQueryDTO(com.clubfactory.query.client.dto.search.SearchRequestDTO searchRequestDTO) {
        return queryAnalyzeService.getQueryDTO(searchRequestDTO);
    }
}
