package com.clubfactory.query.server.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.clubfactory.query.client.dto.analyze.CategoryDTO;
import com.clubfactory.query.client.dto.search.SearchDTO;
import com.clubfactory.query.client.dto.search.SearchRequestDTO;
import com.clubfactory.query.core.dto.category.CategoryAndWeightDTO;
import com.clubfactory.query.core.dto.category.CategoryPredictQuery;
import com.clubfactory.query.core.dto.category.CategoryPredictResponseV2;
import com.clubfactory.query.core.dto.category.LeafCategoryPredictResult;
import com.clubfactory.query.core.dto.consts.SearchRequestConst;
import com.clubfactory.query.core.dto.enums.GenderEnum;
import com.clubfactory.query.core.dto.gender.GenderPredictQuery;
import com.clubfactory.query.core.dto.gender.GenderPredictResponse;
import com.clubfactory.query.core.dto.result.Result;
import com.clubfactory.query.core.utils.Stopwords;
import com.clubfactory.query.server.config.SearchCategoryMapConfig;
import com.clubfactory.query.server.service.QueryChangeService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.Type;
import java.util.*;

/**
 * @Author: cmj
 * @Description:
 * @Date: 2019/2/27
 */
@Slf4j
@Service
public class QueryChangeServiceImpl implements QueryChangeService {

    public static Map<String, GenderEnum> sexMap = new HashMap<>();

    public static final String WORD_EDGE = "\\b";

    private static final String PUNCTUATION_PATTERN = "\\p{P}";

    public static final int KEYWORD_LENGTH_LIMIT = 500;

    private static final Stopwords STOPWORDS = new Stopwords();

    private static final Type CATEGORY_RESULT_TYPE_V2 = new TypeReference<Result<CategoryPredictResponseV2>>() {}.getType();

    private static final Type LEAF_RESULT_TYPE = new TypeReference<Result<LeafCategoryPredictResult>>(){}.getType();

    private static final Type GENDER_RESULT_TYPE = new TypeReference<Result<GenderPredictResponse>>() {}.getType();

    @Autowired
    private SearchRequestConst searchRequestConst;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private QueryChangeService queryChangeService;

    @Override
    public String searchPreProcess(SearchDTO searchDTO) {
        /** keyword预处理 */
        String keyword = searchDTO.getKey();
        keyword = removeURL(keyword);
        keyword = StringUtils.lowerCase(keyword);
        keyword = removeStopWords(keyword);

        /** 性别预处理 */

        StringBuilder keywordSbWithoutGender = new StringBuilder();
        /** 如果搜索词中有明确的指向，则以搜索词为准 */
        for (String s : keyword.split(WORD_EDGE)) {
            if (sexMap.containsKey(s)) {

            } else {
                keywordSbWithoutGender.append(s);
            }
        }
        String keywordWithoutGender = keywordSbWithoutGender.toString();
        if (!StringUtils.isBlank(keywordWithoutGender)) {
            keyword = StringUtils.trim(keywordWithoutGender);
        }

        keyword = removeExtraBlanks(keyword);


        return keyword;
    }

    @Override
    public GenderEnum getSex(SearchDTO searchDTO) {
        /** keyword预处理 */
        String keyword = searchDTO.getKey();
        keyword = removeURL(keyword);
        keyword = StringUtils.lowerCase(keyword);
        keyword = removeStopWords(keyword);
        /** 性别预处理 */
        String genderStr = searchDTO.getSex();
        /** 默认为ALL */
        GenderEnum gender = GenderEnum.getEnumByKey(genderStr).orElse(GenderEnum.All);
        /** 首先通过算法预测query适用的性别 */
        gender = predictGender(keyword, gender);

        /** 如果搜索词中有明确的指向，则以搜索词为准 */
        for (String s : keyword.split("\\b")) {
            if (sexMap.containsKey(s)) {
                gender = sexMap.get(s);
            }
        }
        return gender;
    }

    private String postJson(String url, String jsonStr) {
        HttpHeaders headers = new HttpHeaders();
        MediaType type = MediaType.parseMediaType("application/json; charset=UTF-8");
        headers.setContentType(type);
        HttpEntity<String> entity = new HttpEntity<>(jsonStr, headers);
        String resultStr = restTemplate.postForObject(url, entity, String.class);
        return resultStr;
    }

    public Optional<Result<GenderPredictResponse>> predictGender(GenderPredictQuery genderPredictQuery) {
        try {
            String requestJson = JSON.toJSONString(genderPredictQuery);
            log.info(String.format("predict gender start, request is %s", requestJson));

            String genderPredictResultStr = postJson(searchRequestConst.getGenderPredictUrl(), requestJson);

            Result<GenderPredictResponse> genderPredictResult = JSON.parseObject(genderPredictResultStr, GENDER_RESULT_TYPE);

            return Optional.ofNullable(genderPredictResult);
        } catch (Throwable t) {
            log.error("error requesting search api", t);
        }
        return Optional.empty();
    }

    public GenderEnum predictGender(final String queryStr, final GenderEnum originGender) {

        GenderEnum gender = (originGender != null ? originGender : GenderEnum.All);

        if (StringUtils.isBlank(queryStr)) {
            return gender;
        }

        /** 调用接口预测性别 */
        GenderPredictQuery genderPredictQuery =
                GenderPredictQuery.builder().gender(gender.getKey()).query(queryStr).build();
        Optional<Result<GenderPredictResponse>> option =
                predictGender(genderPredictQuery);
        if (!option.isPresent()) {
            return gender;
        }

        /** 请求失败，返回原性别 */
        Result<GenderPredictResponse> predictResponseResult = option.get();
        if (!predictResponseResult.getSuccess()) {
            return gender;
        }

        GenderPredictResponse genderPredictResponse = predictResponseResult.getData();
        if (genderPredictResponse != null) {
            String genderStr = genderPredictResponse.getGender();
            Optional<GenderEnum> genderEnumOptional = GenderEnum.getEnumByKey(genderStr);
            // 如果有结果则更新， 否则使用原值
            gender = genderEnumOptional.orElse(gender);
        }

        return gender;
    }

    /**
     * 删除关键词之间过多的空格
     *
     * @return
     */
    public  static String removeExtraBlanks(final String keywords) {
        if (StringUtils.isBlank(keywords)) {
            return "";
        }
        StringBuilder resultSb = new StringBuilder();
        /** 如果搜索词中有明确的指向，则以搜索词为准 */
        for (String s : keywords.split(WORD_EDGE)) {
            if (StringUtils.isBlank(s)) {
                continue;
            }
            resultSb.append(s).append(" ");
        }

        return StringUtils.trim(resultSb.toString());
    }

    public static  String removeStopWords(final String keywords) {

        if (StringUtils.isBlank(keywords)) {
            return "";
        }

        String result = keywords;

        try {
            /** 去除标点符号 */
            String keywordsWithoutPunctuate = keywords.replaceAll(PUNCTUATION_PATTERN, " ");

            StringBuilder sb = new StringBuilder(keywordsWithoutPunctuate.length());
            for (String s : keywordsWithoutPunctuate.split(WORD_EDGE)) {
                /** 不是stop word ，则加入，t的情况列外，t shirt*/
                if ("t".equals(s) || (!StringUtils.isBlank(s) && !STOPWORDS.is(s))) {
                    sb.append(s).append(" ");
                }
            }
            result = sb.toString();
        } catch (Exception e) {
            log.error("error removing stop words by lucene");
        }

        if (StringUtils.isBlank(result.toString())) {
            result = keywords;
        }
        return StringUtils.trim(result);
    }

    public static String removeURL(final String keywords) {
        /** 清除空字符串 */
        if (StringUtils.isBlank(keywords)) {
            return "";
        }

        String resultKeywords = keywords;

        /** 截取过长的keywords */
        if (keywords.length() > KEYWORD_LENGTH_LIMIT) {
            resultKeywords = keywords.substring(0, KEYWORD_LENGTH_LIMIT);
        }

        /** 取url前的String */
        String[] splittedKeywords = StringUtils.splitByWholeSeparator(resultKeywords, "http://");
        resultKeywords = splittedKeywords[0];

        splittedKeywords = StringUtils.splitByWholeSeparator(resultKeywords, "https://");
        resultKeywords = splittedKeywords[0];

        /** 去除后为空字符串 则直接返回原有字段 */
        if (StringUtils.isBlank(resultKeywords)) {
            resultKeywords = keywords;
        }

        return StringUtils.trim(resultKeywords);
    }

    /**
     * 解析为1，2，3级类目
     *
     * @param categoryList
     * @return
     */
    private CategoryDTO getCategoryMap(List<Long> categoryList) {
        CategoryDTO categoryDTO = new CategoryDTO();
        Map<String, Double> rootCategoryPredict = new HashMap<>();
        Map<String, Double> secondCategoryPredict = new HashMap<>();
        Map<String, Double> leafCategoryPredict = new HashMap<>();
        for (Long id : categoryList) {
            if (id.toString().length() == 3) {
                rootCategoryPredict.put(id.toString(), 1d);
            } else if (id.toString().length() == 5) {
                secondCategoryPredict.put(id.toString(), 1d);
            } else if (id.toString().length() == 8) {
                leafCategoryPredict.put(id.toString(), 1d);
            }
        }
        categoryDTO.setRootCategoryPredict(rootCategoryPredict);
        categoryDTO.setSecondCategoryPredict(secondCategoryPredict);
        categoryDTO.setLeafCategoryPredict(leafCategoryPredict);
        return categoryDTO;
    }

    @Override
    public CategoryDTO getCategoryMapInfo(SearchRequestDTO searchRequestDTO, SearchDTO searchDTO) {
        String key = searchRequestDTO.getKey();
        String sex = searchRequestDTO.getSex();
        String gender;

        switch (sex) {
            case "man":
                gender = "men";
                break;
            case "woman":
                gender = "women";
                break;
            default:
                gender = "all";
                break;
        }

        if (StringUtils.isNoneEmpty(key)) {
            key = key.toLowerCase();
        }

        List<Long> mapCategoryIds = SearchCategoryMapConfig.searchCategoryMap.get(String.format("%s_%s", key, gender));
        List<Long> limitCategoryIds = SearchCategoryMapConfig.searchLimitCategoryMap.get(String.format("%s_%s", key, gender));
        List<Long> idList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(mapCategoryIds)) {
            idList.addAll(mapCategoryIds);
        }
        if (!CollectionUtils.isEmpty(limitCategoryIds)) {
            idList.addAll(limitCategoryIds);
        }
        //人工映射去重
        idList = new ArrayList<>(new HashSet<>(idList));
        CategoryDTO categoryDTO = getCategoryMap(idList);
        //获取类目预测信息
        CategoryPredictResponseV2 categoryInfo = queryChangeService.getCategoryInfo(searchDTO.getKey(), GenderEnum.getEnumByKey(searchDTO.getSex()));
        LeafCategoryPredictResult leafCategoryPredictResult = queryChangeService.getLeafCategoryInfo(searchDTO.getKey(), GenderEnum.getEnumByKey(searchDTO.getSex()), searchDTO.getNewVersion());
        //合并人工、预测类目
        Map<String, Double> categ_id_dict = categoryInfo.getCateg_id_dict();
        Map<String, Double> category_id_dict = categoryInfo.getCategory_id_dict();
        Map<String, Double> category_leaf_id_dict = leafCategoryPredictResult.getCategory_leaf_id_dict();
        if (!CollectionUtils.isEmpty(categoryDTO.getRootCategoryPredict())) {
            categ_id_dict.putAll(categoryDTO.getRootCategoryPredict());
        }


        categoryDTO.setRootCategoryPredict(categ_id_dict);

        if (category_leaf_id_dict != null) {

            if (!CollectionUtils.isEmpty(categoryDTO.getLeafCategoryPredict())) {
                category_leaf_id_dict.putAll(categoryDTO.getLeafCategoryPredict());
            }
            categoryDTO.setLeafCategoryPredict(category_leaf_id_dict);
            categoryDTO.setSecondCategoryPredict(new HashMap<>());
        } else {
            if (!CollectionUtils.isEmpty(categoryDTO.getSecondCategoryPredict())) {
                category_id_dict.putAll(categoryDTO.getSecondCategoryPredict());
            }
            categoryDTO.setSecondCategoryPredict(category_id_dict);
            categoryDTO.setLeafCategoryPredict(new HashMap<>());
        }
        return categoryDTO;
    }

    /**
     * 二级类目预测
     *
     * @return
     */
    public Optional<Result<CategoryPredictResponseV2>> predictCategoryLevel_2(CategoryPredictQuery categoryPredictQuery) {
        try {
            String requestJson = JSON.toJSONString(categoryPredictQuery);
            log.info(String.format("二级类目预测开始执行, request is %s", requestJson));

            String categoryPredictResultStr = postJson(searchRequestConst.getCategoryPredictUrl(), requestJson);

            Result<CategoryPredictResponseV2> categoryPredictResponse = JSON.parseObject(categoryPredictResultStr, CATEGORY_RESULT_TYPE_V2);

            return Optional.ofNullable(categoryPredictResponse);
        } catch (Throwable t) {
            log.error("二级类目预测开始执行失败", t);
        }
        return Optional.empty();
    }

    @Override
    public CategoryPredictResponseV2 getCategoryInfo(String queryStr, Optional<GenderEnum> originGender) {

        CategoryPredictResponseV2 categoryPredictResponse = new CategoryPredictResponseV2();

        GenderEnum gender = originGender.get();

        if (originGender == null) {
            gender = GenderEnum.All;
        }

        /**
         * query为空，无法预测
         */
        if (StringUtils.isBlank(queryStr)) {
            return categoryPredictResponse;
        }
        CategoryPredictQuery categoryPredictQuery =
                CategoryPredictQuery.builder().gender(gender.getKey()).query(queryStr).version(2).level(2).build();
        Optional<Result<CategoryPredictResponseV2>> option =
                predictCategoryLevel_2(categoryPredictQuery);

        /**
         * 查询失败，返回空列表
         */
        if (!option.isPresent()) {
            log.error(String.format("类目预测失败!", queryStr));
            return categoryPredictResponse;
        }

        /** 请求失败，返回原性别 */
        Result<CategoryPredictResponseV2> predictResponseResult = option.get();
        if (!predictResponseResult.getSuccess()) {
            log.warn(String.format("类目预测失败!", queryStr));
            return categoryPredictResponse;
        }

        categoryPredictResponse = predictResponseResult.getData();
        if (categoryPredictResponse != null) {
        }

        return categoryPredictResponse;
    }

    @Override
    public LeafCategoryPredictResult getLeafCategoryInfo(String queryStr, Optional<GenderEnum> originGender, Integer newVersion) {

        LeafCategoryPredictResult leafCategoryPredictResult=new LeafCategoryPredictResult();

        GenderEnum gender = originGender.get();

        if (originGender == null) {
            gender = GenderEnum.All;
        }

        /**
         * query为空，无法预测
         */
        if (StringUtils.isBlank(queryStr)) {
            return leafCategoryPredictResult;
        }
        CategoryPredictQuery categoryPredictQuery = CategoryPredictQuery.builder().gender(gender.getKey()).query(queryStr).version(2).level(3).build();
        Optional<Result<LeafCategoryPredictResult>> option =predictLeafCategory(categoryPredictQuery);

        /**
         * 查询失败，返回空列表
         */
        if (option==null||!option.isPresent()) {
            log.error(String.format("类目预测失败!", queryStr));
            return leafCategoryPredictResult;
        }

        /** 请求失败，返回原性别 */
        Result<LeafCategoryPredictResult> predictResponseResult = option.get();
        if (!predictResponseResult.getSuccess()) {
            log.warn(String.format("类目预测失败!", queryStr));
            return leafCategoryPredictResult;
        }

        leafCategoryPredictResult = predictResponseResult.getData();

        return leafCategoryPredictResult;
    }

    @Override
    public CategoryAndWeightDTO getCategoryAndWeightDTO() {
        return null;
    }

    /**
     * 叶子类目预测
     * @return
     */
    public Optional<Result<LeafCategoryPredictResult>> predictLeafCategory(CategoryPredictQuery categoryPredictQuery){
        try {
            String requestJson = JSON.toJSONString(categoryPredictQuery);
            log.info(String.format("叶子类目预测, request is %s", requestJson));

            String categoryPredictResultStr = postJson(searchRequestConst.getCategoryPredictUrl(), requestJson) ;

            Result<LeafCategoryPredictResult> categoryPredictResponse  = JSON.parseObject(categoryPredictResultStr, LEAF_RESULT_TYPE);

            return Optional.ofNullable(categoryPredictResponse);
        }catch (Throwable t){
            log.error("叶子类目执行失败", t);
        }
        return Optional.empty();
    }
}
