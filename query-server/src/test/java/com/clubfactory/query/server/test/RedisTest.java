package com.clubfactory.query.server.test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.clubfactory.query.core.utils.TermUtil;
import com.clubfactory.query.server.Application;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @Author: cmj
 * @Description:
 * @Date: 2019/2/26
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@Slf4j
public class RedisTest {
    @Autowired
    TermUtil termUtil;

    @Test
    public void test() {
        JSONObject shoes_0 = termUtil.getTagTerm("search:getQueryTaggingRedisKey", "hoodies for man");
        double hoodies_for_man = termUtil.getTermWeight("search:getTermWeightRedisKey_man", "shoes_0");
        log.info("tag:{},weight:{}",shoes_0,hoodies_for_man);
    }
}
