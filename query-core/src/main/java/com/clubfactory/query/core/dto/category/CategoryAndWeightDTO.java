package com.clubfactory.query.core.dto.category;

import lombok.Data;

import java.util.List;

/**
 * @Author: cmj
 * @Description:
 * @Date: 2019/3/8
 */
@Data
public class CategoryAndWeightDTO {
    private String message;
    private List<CategoryInfo> data;
}
