package com.clubfactory.query.core.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

/**
 * @Author: cmj
 * @Description:
 * @Date: 2019/2/27
 */
@Component
public class TermUtil {

    @Autowired
    RedisTemplate redisTemplate;

    public double getTermWeight(String hash, String key) {
        return Double.valueOf(redisTemplate.opsForHash().get(hash, key).toString());
    }

    public JSONObject getTagTerm(String hash, String key) {
        String tag = redisTemplate.opsForHash().get(hash, key).toString();
        return JSON.parseObject(tag);
    }

}
