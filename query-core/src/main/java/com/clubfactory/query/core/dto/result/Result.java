package com.clubfactory.query.core.dto.result;

import lombok.Data;
import lombok.ToString;

/**
 * @author shenjianan
 * @date 2018/9/29
 */
@Data
@ToString
public class Result<T> {
    private T data;
    private Boolean success;
}
