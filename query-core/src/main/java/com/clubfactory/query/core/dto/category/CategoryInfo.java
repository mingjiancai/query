package com.clubfactory.query.core.dto.category;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @Author: cmj
 * @Description:
 * @Date: 2019/3/8
 */
@Data
public class CategoryInfo {
    private List<String> allCategoryId;
    private List<String> femaleCategoryId;
    private List<String> maleCategoryId;
    private Map perdictAllCategoryId;
    private Map perdictFemaleCategoryId;
    private Map perdictMaleCategoryId;
    private String relation;
    private String searchKey;
}
