package com.clubfactory.query.core.dto.term.corrent;


/**
 * @Author: cmj
 * @Description:
 * @Date: 2019/2/26
 */
@lombok.Data
public class TermResponse {
    private Data data;
    private int code;
}
