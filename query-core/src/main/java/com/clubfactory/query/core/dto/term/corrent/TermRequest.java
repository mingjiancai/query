package com.clubfactory.query.core.dto.term.corrent;

import lombok.Data;

/**
 * @author cmj
 */
@Data
public class TermRequest {

    private String query;
    private int version;


}