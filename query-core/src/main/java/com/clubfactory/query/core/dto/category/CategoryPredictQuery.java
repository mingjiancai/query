package com.clubfactory.query.core.dto.category;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

/**
 * @author shenjianan
 * @date 2018/9/29
 */
@Data
@ToString
@Builder
public class CategoryPredictQuery {
    private String query;
    private String gender;
    private int version;
    private int level;
}
