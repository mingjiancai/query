package com.clubfactory.query.core.dto.category;

import lombok.Data;

import java.util.Map;

/**
 * @Author: cmj
 * @Description:
 * @Date: 2019/1/28
 */
@Data
public class LeafCategoryPredictResult {
    private Map<String,Double> categ_id_dict;
    private Map<String,Double> category_leaf_id_dict;
}
