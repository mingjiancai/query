package com.clubfactory.query.core.dto.term.corrent;

import java.util.HashMap;
import java.util.List;

/**
 * @Author: cmj
 * @Description:
 * @Date: 2019/2/26
 */
@lombok.Data
public class Data {
    private String query;
    private List<HashMap> correcting_dic;
}
