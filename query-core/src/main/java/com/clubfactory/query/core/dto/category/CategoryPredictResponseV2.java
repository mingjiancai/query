package com.clubfactory.query.core.dto.category;

import lombok.Data;
import lombok.ToString;

import java.util.Map;

@Data
@ToString
public class CategoryPredictResponseV2 {
    private Map<String,Double> categ_id_dict;
    private Map<String,Double> category_id_dict;
}
