package com.clubfactory.query.core.dto.enums;

/**
 * 不同的类别映射方式
 *
 * @author shenjianan
 * @date 2018/10/10
 */
public enum CategoryMappingType {
  /** 取出此类别中的所有商品 */
  EQUAL,
  /** 限制在此类别中进行搜索 */
  LIMIT;
}
