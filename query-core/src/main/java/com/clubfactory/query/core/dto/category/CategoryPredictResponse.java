package com.clubfactory.query.core.dto.category;

import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * @author shenjianan
 * @date 2018/9/29
 */
@Data
@ToString
public class CategoryPredictResponse {
    private List<Long> categIdList;
}
