package com.clubfactory.query.core.dto.enums;

import org.apache.commons.lang3.StringUtils;

import java.util.Optional;

/**
 * @author shenjianan
 * @date 2018/9/29
 */
public enum GenderEnum {
    Man("man"),
    Woman("woman"),
    All("all");


    private String key;

    GenderEnum(String key){
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public static Optional<GenderEnum> getEnumByKey(String str){
        if (!StringUtils.isEmpty(str)) {
            for (GenderEnum value : values()) {
                if (value.getKey().equals(str)) {
                    return Optional.of(value);
                }
            }
        }

        return Optional.empty();
    }
}
