//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.clubfactory.query.core.utils;

import weka.core.RevisionHandler;
import weka.core.RevisionUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Vector;

public class Stopwords implements RevisionHandler {
    protected HashSet<String> m_Words = null;
    protected static Stopwords m_Stopwords;

    public Stopwords() {
        this.m_Words = new HashSet();
        this.add("a");
        this.add("able");
        this.add("according");
        this.add("accordingly");
        this.add("actually");
        this.add("after");
        this.add("afterwards");
        this.add("again");
        this.add("against");
        this.add("all");
        this.add("allow");
        this.add("allows");
        this.add("almost");
        this.add("alone");
        this.add("along");
        this.add("already");
        this.add("also");
        this.add("although");
        this.add("always");
        this.add("am");
        this.add("among");
        this.add("amongst");
        this.add("an");
        this.add("and");
        this.add("another");
        this.add("anybody");
        this.add("anyhow");
        this.add("anyone");
        this.add("anything");
        this.add("anyway");
        this.add("anyways");
        this.add("anywhere");
        this.add("apart");
        this.add("appear");
        this.add("appreciate");
        this.add("appropriate");
        this.add("are");
        this.add("around");
        this.add("as");
        this.add("aside");
        this.add("ask");
        this.add("asking");
        this.add("associated");
        this.add("at");
        this.add("available");
        this.add("away");
        this.add("awfully");
        this.add("b");
        this.add("be");
        this.add("became");
        this.add("because");
        this.add("become");
        this.add("becomes");
        this.add("becoming");
        this.add("been");
        this.add("before");
        this.add("beforehand");
        this.add("behind");
        this.add("being");
        this.add("believe");
        this.add("below");
        this.add("beside");
        this.add("besides");
        this.add("between");
        this.add("beyond");
        this.add("both");
        this.add("brief");
        this.add("but");
        this.add("by");
        this.add("c");
        this.add("came");
        this.add("can");
        this.add("cannot");
        this.add("cant");
        this.add("cause");
        this.add("causes");
        this.add("certain");
        this.add("certainly");
        this.add("changes");
        this.add("clearly");
        this.add("co");
        this.add("com");
        this.add("come");
        this.add("comes");
        this.add("concerning");
        this.add("consequently");
        this.add("consider");
        this.add("considering");
        this.add("contain");
        this.add("containing");
        this.add("contains");
        this.add("corresponding");
        this.add("could");
        this.add("course");
        this.add("currently");
        this.add("d");
        this.add("definitely");
        this.add("described");
        this.add("despite");
        this.add("did");
        this.add("different");
        this.add("do");
        this.add("does");
        this.add("doing");
        this.add("done");
        this.add("down");
        this.add("downwards");
        this.add("during");
        this.add("e");
        this.add("each");
        this.add("eg");
        this.add("either");
        this.add("else");
        this.add("elsewhere");
        this.add("enough");
        this.add("entirely");
        this.add("especially");
        this.add("et");
        this.add("etc");
        this.add("even");
        this.add("ever");
        this.add("every");
        this.add("everybody");
        this.add("everyone");
        this.add("everything");
        this.add("everywhere");
        this.add("ex");
        this.add("exactly");
        this.add("example");
        this.add("except");
        this.add("f");
        this.add("far");
        this.add("few");
        this.add("followed");
        this.add("following");
        this.add("follows");
        this.add("for");
        this.add("former");
        this.add("formerly");
        this.add("from");
        this.add("further");
        this.add("furthermore");
        this.add("g");
        this.add("get");
        this.add("gets");
        this.add("getting");
        this.add("given");
        this.add("gives");
        this.add("go");
        this.add("goes");
        this.add("going");
        this.add("gone");
        this.add("got");
        this.add("gotten");
        this.add("greetings");
        this.add("h");
        this.add("had");
        this.add("happens");
        this.add("hardly");
        this.add("has");
        this.add("have");
        this.add("having");
        this.add("he");
        this.add("hello");
        this.add("help");
        this.add("hence");
        this.add("her");
        this.add("here");
        this.add("hereafter");
        this.add("hereby");
        this.add("herein");
        this.add("hereupon");
        this.add("hers");
        this.add("herself");
        this.add("hi");
        this.add("himself");
        this.add("his");
        this.add("hither");
        this.add("hopefully");
        this.add("how");
        this.add("howbeit");
        this.add("however");
        this.add("i");
        this.add("ie");
        this.add("if");
        this.add("ignored");
        this.add("immediate");
        this.add("in");
        this.add("inasmuch");
        this.add("inc");
        this.add("indeed");
        this.add("indicate");
        this.add("indicated");
        this.add("indicates");
        this.add("inner");
        this.add("insofar");
        this.add("instead");
        this.add("into");
        this.add("inward");
        this.add("is");
        this.add("it");
        this.add("its");
        this.add("itself");
        this.add("j");
        this.add("just");
        this.add("k");
        this.add("keep");
        this.add("keeps");
        this.add("kept");
        this.add("know");
        this.add("knows");
        this.add("known");
        this.add("l");
        this.add("last");
        this.add("lately");
        this.add("later");
        this.add("latter");
        this.add("latterly");
        this.add("least");
        this.add("less");
        this.add("lest");
        this.add("let");
        this.add("like");
        this.add("liked");
        this.add("likely");
        this.add("little");
        this.add("ll");
        this.add("looking");
        this.add("looks");
        this.add("ltd");
        this.add("m");
        this.add("mainly");
        this.add("many");
        this.add("may");
        this.add("maybe");
        this.add("meanwhile");
        this.add("merely");
        this.add("might");
        this.add("more");
        this.add("moreover");
        this.add("mostly");
        this.add("much");
        this.add("must");
        this.add("my");
        this.add("myself");
        this.add("n");
        this.add("namely");
        this.add("nd");
        this.add("near");
        this.add("nearly");
        this.add("need");
        this.add("needs");
        this.add("neither");
        this.add("nevertheless");
        this.add("non");
        this.add("none");
        this.add("noone");
        this.add("nor");
        this.add("normally");
        this.add("not");
        this.add("o");
        this.add("obviously");
        this.add("of");
        this.add("off");
        this.add("often");
        this.add("oh");
        this.add("ok");
        this.add("okay");
        this.add("on");
        this.add("once");
        this.add("one");
        this.add("ones");
        this.add("only");
        this.add("onto");
        this.add("or");
        this.add("other");
        this.add("others");
        this.add("otherwise");
        this.add("ought");
        this.add("out");
        this.add("outside");
        this.add("own");
        this.add("p");
        this.add("particularly");
        this.add("per");
        this.add("perhaps");
        this.add("placed");
        this.add("possible");
        this.add("presumably");
        this.add("probably");
        this.add("provides");
        this.add("q");
        this.add("que");
        this.add("quite");
        this.add("qv");
        this.add("r");
        this.add("rather");
        this.add("rd");
        this.add("re");
        this.add("really");
        this.add("reasonably");
        this.add("regarding");
        this.add("regardless");
        this.add("regards");
        this.add("relatively");
        this.add("respectively");
        this.add("right");
        this.add("s");
        this.add("said");
        this.add("same");
        this.add("say");
        this.add("saying");
        this.add("says");
        this.add("see");
        this.add("seeing");
        this.add("seem");
        this.add("seemed");
        this.add("seeming");
        this.add("seems");
        this.add("seen");
        this.add("self");
        this.add("selves");
        this.add("sensible");
        this.add("sent");
        this.add("serious");
        this.add("seriously");
        this.add("several");
        this.add("shall");
        this.add("should");
        this.add("since");
        this.add("so");
        this.add("soon");
        this.add("specified");
        this.add("specify");
        this.add("specifying");
        this.add("still");
        this.add("such");
        this.add("sup");
        this.add("sure");
        this.add("t");
        this.add("take");
        this.add("taken");
        this.add("tell");
        this.add("tends");
        this.add("th");
        this.add("than");
        this.add("thanx");
        this.add("that");
        this.add("thats");
        this.add("the");
        this.add("their");
        this.add("theirs");
        this.add("them");
        this.add("themselves");
        this.add("then");
        this.add("thence");
        this.add("there");
        this.add("thereafter");
        this.add("thereby");
        this.add("therefore");
        this.add("therein");
        this.add("theres");
        this.add("thereupon");
        this.add("these");
        this.add("they");
        this.add("this");
        this.add("thorough");
        this.add("thoroughly");
        this.add("those");
        this.add("though");
        this.add("through");
        this.add("throughout");
        this.add("thru");
        this.add("thus");
        this.add("together");
        this.add("too");
        this.add("took");
        this.add("toward");
        this.add("towards");
        this.add("tried");
        this.add("tries");
        this.add("try");
        this.add("trying");
        this.add("u");
        this.add("un");
        this.add("under");
        this.add("unfortunately");
        this.add("unless");
        this.add("unlikely");
        this.add("until");
        this.add("unto");
        this.add("up");
        this.add("upon");
        this.add("uses");
        this.add("using");
        this.add("usually");
        this.add("uucp");
        this.add("v");
        this.add("ve");
        this.add("very");
        this.add("via");
        this.add("viz");
        this.add("vs");
        this.add("w");
        this.add("wants");
        this.add("was");
        this.add("way");
        this.add("went");
        this.add("were");
        this.add("what");
        this.add("whatever");
        this.add("when");
        this.add("whence");
        this.add("whenever");
        this.add("where");
        this.add("whereafter");
        this.add("whereas");
        this.add("whereby");
        this.add("wherein");
        this.add("whereupon");
        this.add("wherever");
        this.add("whether");
        this.add("which");
        this.add("while");
        this.add("whither");
        this.add("whoever");
        this.add("whole");
        this.add("whom");
        this.add("whose");
        this.add("with");
        this.add("within");
        this.add("without");
        this.add("wonder");
        this.add("would");
        this.add("would");
        this.add("x");
        this.add("y");
        this.add("yet");
        this.add("z");
    }

    public void clear() {
        this.m_Words.clear();
    }

    public void add(String word) {
        if (word.trim().length() > 0) {
            this.m_Words.add(word.trim().toLowerCase());
        }

    }

    public boolean remove(String word) {
        return this.m_Words.remove(word);
    }

    public boolean is(String word) {
        return this.m_Words.contains(word.toLowerCase());
    }

    public Enumeration<String> elements() {
        Vector<String> list = new Vector();
        list.addAll(this.m_Words);
        Collections.sort(list);
        return list.elements();
    }

    public void read(String filename) throws Exception {
        this.read(new File(filename));
    }

    public void read(File file) throws Exception {
        this.read(new BufferedReader(new FileReader(file)));
    }

    public void read(BufferedReader reader) throws Exception {
        this.clear();

        String line;
        while ((line = reader.readLine()) != null) {
            line = line.trim();
            if (!line.startsWith("#")) {
                this.add(line);
            }
        }

        reader.close();
    }

    public void write(String filename) throws Exception {
        this.write(new File(filename));
    }

    public void write(File file) throws Exception {
        this.write(new BufferedWriter(new FileWriter(file)));
    }

    public void write(BufferedWriter writer) throws Exception {
        writer.write("# generated " + new Date());
        writer.newLine();
        Enumeration enm = this.elements();

        while (enm.hasMoreElements()) {
            writer.write(((String) enm.nextElement()).toString());
            writer.newLine();
        }

        writer.flush();
        writer.close();
    }
    @Override
    public String toString() {
        StringBuffer result = new StringBuffer();
        Enumeration enm = this.elements();

        while (enm.hasMoreElements()) {
            result.append(((String) enm.nextElement()).toString());
            if (enm.hasMoreElements()) {
                result.append(",");
            }
        }

        return result.toString();
    }

    public static boolean isStopword(String str) {
        return m_Stopwords.is(str.toLowerCase());
    }


    static {
        if (m_Stopwords == null) {
            m_Stopwords = new Stopwords();
        }

    }

    @Override
    public String getRevision() {
        return RevisionUtils.extract("$Revision: 10203 $");
    }
}
