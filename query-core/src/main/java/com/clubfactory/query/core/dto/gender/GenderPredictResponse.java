package com.clubfactory.query.core.dto.gender;

import lombok.Data;
import lombok.ToString;

/**
 * @author shenjianan
 * @date 2018/9/29
 */
@Data
@ToString
public class GenderPredictResponse {
    private String gender;
}
