package com.clubfactory.query.core.dto.gender;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

/**
 * @author shenjianan
 * @date 2018/9/29
 */
@Data
@ToString
@Builder
public class GenderPredictQuery {
    private String query;
    private String gender;
}
