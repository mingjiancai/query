package com.clubfactory.query.core.dto.consts;

import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author shenjianan
 * @date 2018/7/31
 */
@Slf4j
@Data
@ToString
@Component
public class SearchRequestConst {

    @Value("${search.broker_url}" +"/predict/category")
    private String categoryPredictUrl;

    @Value("${search.broker_url}" +"/predict/gender")
    private String genderPredictUrl;

    @Value("${search.broker_url}" +"/admin/query_rewrite")
    private String queryRewrite;

    @Value("${search.broker_url}" +"/get_term_weight")
    private String wordWeight;

    @Value("${search.broker_url}" +"/admin/query_process")
    private String queryAnalyze;

    @Value("${search.REQUEST_ES_TIMEOUT_MILLI_SECS}")
    private Integer REQUEST_ES_TIMEOUT_MILLI_SECS;

    @Value("${search.REQUEST_ES_CONNECT_TIMEOUT_MILLI_SECS}")
    private Integer REQUEST_ES_CONNECT_TIMEOUT_MILLI_SECS;

    @Value("${search.REQUEST_ES_READ_TIMEOUT_MILLI_SECS}")
    private Integer REQUEST_ES_READ_TIMEOUT_MILLI_SECS;

    @Value("${search.HTTP_MAX_CONNECTION_COUNT}")
    private Integer HTTP_MAX_CONNECTION_COUNT;

    @Value("${search.SEARCH_HOST_MAX_CONNECTION_COUNT}")
    private Integer SEARCH_HOST_MAX_CONNECTION_COUNT;

    @Value("${search.USE_CACHE}")
    private Boolean USE_CACHE;

    @Value("${search.DISASTER_TOLERANCE}")
    private Boolean disasterTolerance;

    @PostConstruct
    public void print(){
        log.info("=============================");
        log.info(this.toString());
        log.info("=============================");
    }
}
