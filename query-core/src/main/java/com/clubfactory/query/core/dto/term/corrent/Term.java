package com.clubfactory.query.core.dto.term.corrent;

import lombok.Data;

/**
 * @Author: cmj
 * @Description:
 * @Date: 2019/2/26
 */
@Data
public class Term {
    String term;
}
