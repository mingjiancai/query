package com.clubfactory.query.client.dto.request;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class PropertyFilterDTO implements Serializable {
    private static final long serialVersionUID = 13614228123554924L;

    private String key;

    private List<String> values;
}
