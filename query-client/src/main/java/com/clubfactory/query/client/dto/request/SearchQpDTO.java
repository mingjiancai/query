package com.clubfactory.query.client.dto.request;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@Data
public class SearchQpDTO implements Serializable {
    private static final long serialVersionUID = 13614228123554924L;

    /**
     * 曝光请求id
     */
    private String pvid;
    /**
     * 用户输入原始query
     */
    private String originQuery;
    /**
     * 用户信息
     */
    private UserInfoDTO userInfo;
    /**
     * 用户输入过滤条件
     */
    private UserFilterDTO userFilter;
    /**
     * 类目预测信息
     */
    private CategoryPredictDTO categoryPredict;
    /**
     * 关键词分析
     */
    private QueryAnalyzeDTO queryAnalyze;
    /**
     * 用户指定排序：sortKey: isAsc
     */
    private Map<String, Boolean> sortMap;
    /**
     * 版本号
     */
    private Integer version;
    /**
     * 是否debug请求
     */
    private Boolean debug;
}
