package com.clubfactory.query.client.dto.analyze;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: cmj
 * @Description: query分析请求体
 * @Date: 2019/1/15
 */
@Data
public class QueryAnalyzeRequest implements Serializable {
    private static final long serialVersionUID = -5856297567466813067L;
    private Ab ab;
    private User user;
    private Input input;
    private String query;
}

