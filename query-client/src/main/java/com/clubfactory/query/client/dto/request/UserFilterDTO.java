package com.clubfactory.query.client.dto.request;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class UserFilterDTO implements Serializable {
    private static final long serialVersionUID = -1;
    /**
     * 颜色过滤
     */
    private List<String> color;
    /**
     * 尺码过滤
     */
    private List<String> size;
    /**
     * 价格区间
     */
    private PriceFilterDTO price;
    /**
     * 普通属性过滤
     */
    private List<PropertyFilterDTO> property;
    /**
     * 类目过滤
     */
    private List<Integer> category;
}
