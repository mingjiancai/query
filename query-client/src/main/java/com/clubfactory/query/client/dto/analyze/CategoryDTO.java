package com.clubfactory.query.client.dto.analyze;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * @Author: cmj
 * @Description:
 * @Date: 2019/1/17
 */
@Data
public class CategoryDTO implements Serializable {
    private static final long serialVersionUID = 6268460212790931240L;
    private Map<String, Double> rootCategoryPredict;
    private Map<String, Double> secondCategoryPredict;
    private Map<String, Double> leafCategoryPredict;
}
