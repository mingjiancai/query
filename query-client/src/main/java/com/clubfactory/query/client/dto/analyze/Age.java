package com.clubfactory.query.client.dto.analyze;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class Age implements Serializable {

    private static final long serialVersionUID = -6860815081953637141L;
    private Years years;
    private List<String> type;

}