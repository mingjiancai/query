package com.clubfactory.query.client.dto.search;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author: cmj
 * @Description:
 * @Date: 2018/12/11
 */
@Data
public class SubQueryDTO implements Serializable {
    private static final long serialVersionUID = 3908407037969872791L;
    @NotNull
    private String key;
}
