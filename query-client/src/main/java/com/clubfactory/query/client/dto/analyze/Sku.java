package com.clubfactory.query.client.dto.analyze;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class Sku implements Serializable {
    private static final long serialVersionUID = 4688838827724635023L;
    private List<String> color ;
    private List<String> size ;

}