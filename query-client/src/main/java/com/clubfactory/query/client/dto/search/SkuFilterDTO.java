package com.clubfactory.query.client.dto.search;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class SkuFilterDTO implements Serializable {

    private static final long serialVersionUID = -8908106478286676377L;
    private String attrName;

    private String attrValue;

}
