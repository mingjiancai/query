package com.clubfactory.query.client.dto.search;

import lombok.Data;

import java.io.Serializable;

@Data
public class SearchCategoryDTO implements Serializable {

    private static final long serialVersionUID = -2193562922311531223L;
    private String searchKey;

    private String searchCount;

    private String relation;

    private String femaleCategory;

    private String femaleCategoryId;

    private String maleCategory;

    private String maleCategoryId;

    private String allCategory;

    private String allCategoryId;

}
