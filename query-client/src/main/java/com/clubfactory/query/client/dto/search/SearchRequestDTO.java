package com.clubfactory.query.client.dto.search;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Data
public class SearchRequestDTO implements Serializable {

    private static final long serialVersionUID = -3500549110826800643L;
    private String key;

    private Long offset;

    private Long pageSize;

    private Integer newVersion;

    private Boolean searchProductNo;

    private Boolean needCheckCanShipped2me;

    private String sex;

    private Long userId;

    private String cid;

    private Map<String, Boolean> sortMap;

    private Integer content;

    private List<Long> category; // 二级类目id

    private List<Long> cate_id; // 一级类目id

    private List<SkuFilterDTO> sku_filters;

    private List<PriceFilterDTO> price_filters;

    private String country_code;

    private Boolean isBebug;

    private String pvid;

}
