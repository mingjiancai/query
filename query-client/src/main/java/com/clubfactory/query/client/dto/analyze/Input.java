package com.clubfactory.query.client.dto.analyze;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class Input implements Serializable {
    private static final long serialVersionUID = -959975519475688774L;
    private List<Long> category;
    private Sku sku;
    private String pred_gender;
}