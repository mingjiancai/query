package com.clubfactory.query.client.dto.enums;

public enum GenderEnum {
    MAN("man"),
    WOMEN("women"),
    ALL("all"),
    UNKNOWN("unknown");

    private String gender;

    public String getGender() {
        return gender;
    }

    private GenderEnum(String gender) {
        this.gender = gender;
    }

    public static GenderEnum getByString(String genderStr) {
        for (GenderEnum genderEnum : GenderEnum.values()) {
            if (genderEnum.getGender().equals(genderStr)) {
                return genderEnum;
            }
        }
        return GenderEnum.UNKNOWN;
    }
}
