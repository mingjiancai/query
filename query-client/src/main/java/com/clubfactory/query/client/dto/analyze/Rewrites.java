package com.clubfactory.query.client.dto.analyze;


import lombok.Data;

import java.io.Serializable;

@Data
public class Rewrites implements Serializable {

    private static final long serialVersionUID = -6803118663049868964L;
    private int score;
    private Query query;

}