package com.clubfactory.query.client.dto.request;

import lombok.Data;

import java.io.Serializable;

@Data
public class PriceFilterDTO implements Serializable {
    private static final long serialVersionUID = 13614228123554924L;
    private Double from;
    private Double to;
}
