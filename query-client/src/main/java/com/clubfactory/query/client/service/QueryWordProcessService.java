package com.clubfactory.query.client.service;

import com.clubfactory.query.client.dto.request.SearchQpDTO;
import com.clubfactory.query.client.dto.search.SearchRequestDTO;

/**
 * @Author: cmj
 * @Description:
 * @Date: 2019/2/26
 */
public interface QueryWordProcessService {
    SearchQpDTO getQueryDTO(SearchRequestDTO searchRequestDTO);

}
