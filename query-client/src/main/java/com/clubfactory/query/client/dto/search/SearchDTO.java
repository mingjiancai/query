package com.clubfactory.query.client.dto.search;

import lombok.ToString;

import java.io.Serializable;

@ToString
public class SearchDTO  extends SearchRequestDTO implements Serializable {

    private static final long serialVersionUID = -4518761364680883141L;
    private String searchKey;

    public void setSearchKey(String searchKey) {
        this.searchKey = searchKey;
    }

    public String getSearchKey() {
        return searchKey;
    }

    public SearchDTO() {

    }

    public SearchDTO(SearchRequestDTO searchRequestDTO) {
        setKey(searchRequestDTO.getKey());
        setOffset(searchRequestDTO.getOffset());
        setPageSize(searchRequestDTO.getPageSize());
        setNewVersion(searchRequestDTO.getNewVersion());
        setSearchProductNo(searchRequestDTO.getSearchProductNo());
        setNeedCheckCanShipped2me(searchRequestDTO.getNeedCheckCanShipped2me());

        if ("".equals(searchRequestDTO.getSex())){
           setSex("all");
        }
        else {
            setSex(searchRequestDTO.getSex());
        }

        setUserId(searchRequestDTO.getUserId());
        setSortMap(searchRequestDTO.getSortMap());
        setContent(searchRequestDTO.getContent());
        setCate_id(searchRequestDTO.getCate_id());
        setCategory(searchRequestDTO.getCategory());
        setCid(searchRequestDTO.getCid());
        setSku_filters(searchRequestDTO.getSku_filters());
        setPrice_filters(searchRequestDTO.getPrice_filters());
        setCountry_code(searchRequestDTO.getCountry_code());
    }

    @Override
    public SearchDTO clone() {
        SearchDTO searchDTO = null;
        try {
            searchDTO = (SearchDTO)super.clone();
        }
        catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return searchDTO;
    }

}
