package com.clubfactory.query.client.dto.analyze;

import lombok.Data;

import java.io.Serializable;
import java.util.List;


@Data
public class Query implements Serializable {

    private static final long serialVersionUID = 6908562421537761251L;
    private List<String> must;
    private List<List<String>> should;
    private List<String> fuzz;

}