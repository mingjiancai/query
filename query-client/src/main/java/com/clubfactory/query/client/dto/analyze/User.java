package com.clubfactory.query.client.dto.analyze;

import lombok.Data;

import java.io.Serializable;

@Data
public class User implements Serializable {
    private static final long serialVersionUID = 1443842727199379598L;
    private String courtry;
    private String gender;
    private String ip;
    private String city;
    private String uid;
    private String cid;

}