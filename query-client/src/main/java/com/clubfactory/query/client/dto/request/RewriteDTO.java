package com.clubfactory.query.client.dto.request;

import lombok.Data;

import java.io.Serializable;

@Data
public class RewriteDTO implements Serializable {
    private static final long serialVersionUID = 13614228123554924L;
    private Double score;
    private QueryKeyDTO query;
}
