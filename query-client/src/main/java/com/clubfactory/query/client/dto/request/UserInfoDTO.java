package com.clubfactory.query.client.dto.request;

import com.clubfactory.query.client.dto.enums.GenderEnum;
import lombok.Data;

import java.io.Serializable;

@Data
public class UserInfoDTO implements Serializable {
    private static final long serialVersionUID = 13614228123554924L;
    /**
     * uid
     */
    private Long uid;
    /**
     * cid
     */
    private String cid;
    /**
     * 国家
     */
    private String country;
    /**
     * 性别
     */
    private GenderEnum gender;
}
