package com.clubfactory.query.client.dto.request;

import lombok.Data;

import java.io.Serializable;

@Data
public class YearDTO implements Serializable {
    private static final long serialVersionUID = 13614228123554924L;
    private Integer from;
    private Integer to;
}
