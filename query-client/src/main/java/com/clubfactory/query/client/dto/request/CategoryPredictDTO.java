package com.clubfactory.query.client.dto.request;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@Data
public class CategoryPredictDTO implements Serializable {
    private static final long serialVersionUID = -1;

    private Map<String, Double> rootCategoryPredict;
    private Map<String, Double> secondCategoryPredict;
    private Map<String, Double> leafCategoryPredict;
    private Double rootBoost; // 根类目加权
    private Double secondBoost; // 二级类目加权
    private Double leafBoost; // 叶子类目加权
}
