package com.clubfactory.query.client.dto.analyze;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class QueryAnalyzeResponse implements Serializable {

    private static final long serialVersionUID = -3408201596476921264L;
    private List<String> color;
    private List<String> size;
    private List<String> product;
    private List<String> brand;
    private Price price;
    private Age age;
    private List<Rewrites> rewrites;

}