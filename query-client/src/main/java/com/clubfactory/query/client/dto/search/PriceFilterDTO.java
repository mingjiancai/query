package com.clubfactory.query.client.dto.search;

import lombok.Data;

import java.io.Serializable;

@Data
public class PriceFilterDTO implements Serializable {

    private static final long serialVersionUID = 2912844909085656040L;
    private Double lte;

    private Double gte;

//    public PriceFilterDTO(Double lte, Double gte) {
//        this.lte = lte;
//        this.gte = gte;
//    }
}
