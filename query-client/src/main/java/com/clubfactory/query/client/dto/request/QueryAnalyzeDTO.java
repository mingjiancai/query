package com.clubfactory.query.client.dto.request;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class QueryAnalyzeDTO implements Serializable {
    private static final long serialVersionUID = 13614228123554924L;

    private List<String> color;
    private List<String> size;
    private List<String> product;
    private List<String> brand;
    private PriceFilterDTO price;
    private AgeDTO age;
    private List<RewriteDTO> rewrites;
}
