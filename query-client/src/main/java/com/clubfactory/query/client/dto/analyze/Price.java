package com.clubfactory.query.client.dto.analyze;

import lombok.Data;

import java.io.Serializable;

@Data
public class Price implements Serializable {

    private static final long serialVersionUID = -7195243790785897790L;
    private String from;
    private String to;

}