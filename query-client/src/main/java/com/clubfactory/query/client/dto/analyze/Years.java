package com.clubfactory.query.client.dto.analyze;


import lombok.Data;

import java.io.Serializable;

@Data
public class Years implements Serializable {

    private static final long serialVersionUID = -6696994603554154151L;
    private String from;
    private String to;

}