package com.clubfactory.query.client.dto.request;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class QueryKeyDTO implements Serializable {
    private static final long serialVersionUID = 13614228123554924L;

    private List<String> must;
    private List<List<String>> should;
    private List<String> fuzz;

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("QueryKeyDTO{");
        List<String> fieldsList = new ArrayList<>();
        if (must != null && must.size() > 0) {
            fieldsList.add("must=" + must);
        }
        if (should != null && should.size() > 0) {
            fieldsList.add("should=" + should);
        }
        if (fuzz != null && fuzz.size() > 0) {
            fieldsList.add("fuzz=" + fuzz);
        }
        return stringBuilder.append(fieldsList.stream().collect(Collectors.joining(",")))
                .append("}").toString();
    }
}
